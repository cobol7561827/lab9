       IDENTIFICATION DIVISION.
       PROGRAM-ID. EDIT4.
       AUTHOR. NARACHAI.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  STARTS            PIC ******.
       01  NUM-OF-STARTS     PIC 9.
       01  NUM1              PIC VP(5)999.

       PROCEDURE DIVISION.
           PERFORM  VARYING NUM-OF-STARTS FROM 0 BY 1 
                    UNTIL NUM-OF-STARTS  > 5
                    COMPUTE STARTS = 10 ** (4 - NUM-OF-STARTS)
      *             INSPECT STARTS REPLACING ALL "10" BY SPACES
                    INSPECT STARTS CONVERTING "01" TO SPACES 
                    DISPLAY NUM-OF-STARTS " = " STARTS 
                    MOVE ".12345678" TO NUM1 
                    DISPLAY NUM1
           END-PERFORM
           GOBACK.
